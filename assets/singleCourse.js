let params = new URLSearchParams(window.location.search);

let courseId = params.get('courseId')

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollmentContainer = document.querySelector('#enrollmentContainer');

let token = localStorage.getItem('token');
console.log(courseId)
fetch(`http://localhost:3000/api/course/${courseId}`,
	{
		method : "GET",
		headers : {	
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	courseName.innerHTML = result.name;
	courseDesc.innerHTML = result.description;
	coursePrice.innerHTML = result.price;
	enrollmentContainer.innerHTML = 
	`
		<button class="btn btn-primary btn-block">Enroll</button>
	`
})