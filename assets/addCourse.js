//target the form
//listen to an evvent
//get the values
//use fetch to send data to the server
//when response is recieved, alert "course successfully created", else alert "course creation failed something went wrong"
//if success redirect to courses.html 


let addCourseForm = document.getElementById('createCourse');


addCourseForm.addEventListener('submit', (e) => {

	e.preventDefault();



	courseName = document.getElementById('courseName').value;
	coursePrice = document.getElementById('coursePrice').value;
	courseDesc = document.getElementById('courseDesc').value;

	let token = localStorage.getItem("token")

	fetch("http://localhost:3000/api/course/addCourse", {

		method: "POST",
		headers :{
			"Content-Type": "Application/json",
			"Authorization": `Bearer ${token}`
		}, 
		body: JSON.stringify({
			name: courseName,
			price: coursePrice,
			description: courseDesc

		})
	})
	.then( result => result.json())
	.then( result => {
		if(result === true){
			alert("Something went wrong")
		}
		else {
			alert("Course Successfully created")
			window.location.replace('./courses.html')
		}
	})

}) 