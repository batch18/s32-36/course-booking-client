
let editForm = document.querySelector('#editCourse');

let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId);

let token = localStorage.getItem('token');

let courseName = document.querySelector('#courseName');
let coursePrice = document.querySelector('#coursePrice');
let courseDesc = document.querySelector('#courseDesc');

fetch(`http://localhost:3000/api/course/${courseId}`,
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

	courseName.value = result.name
	coursePrice.value = result.price
	courseDesc.value = result.description
})


editForm.addEventListener("submit", (e) => {
	e.preventDefault()

	courseName = courseName.value
	courseDesc = courseDesc.value
	coursePrice = coursePrice.value
	console.log("1")
	fetch(`http://localhost:3000/api/course/edit/${courseId}`,
		{
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: courseDesc,
				price: coursePrice
			})
		}
	)
	.then(result => result.json())
	.then( result => {
		console.log(result)
    if(result !== "undefined"){
			alert (`Course Successfully Updated`)

			window.location.replace("./courses.html")
		} else {
			alert ("Something Went Wrong")
		}
	})
})