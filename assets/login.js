let loginUser = document.getElementById('loginUser');

loginUser.addEventListener('submit', (e) => {

	e.preventDefault();

	email = document.querySelector('#email').value;
	password = document.querySelector('#password').value;

	if(email === "" || password ===""){
		alert (`please input required field`)
	}
	else {
		fetch("http://localhost:3000/api/users/login", {
		    method: "post",
		    headers: { "Content-Type": "application/json" },
		    body: JSON.stringify({ 
		    	email: email, 
		    	password: password 
		    })
		})
		.then(result => result.json())
		.then(result => {

			localStorage.setItem("token", result.access);
			if(result.access){
				fetch("http://localhost:3000/api/users/details", {
					method: "GET",
					headers: {
						"Authorization": `bearer ${result.access}`
					}
				})
				.then(result => result.json())
				.then(result => {
					localStorage.setItem("token-id", result._id);
					localStorage.setItem("token-isAdmin", result.isAdmin);

					alert("Logged in Successfully")
					window.location.replace('./../pages/courses.html')

				})

				/*1. save id, isAdmin to local storage

				2. transfer to courses.html*/

			}

		})
	}
})