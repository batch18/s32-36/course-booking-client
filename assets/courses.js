let token = localStorage.getItem("token");
let isAdmin = localStorage.getItem("token-isAdmin") === "true";
let adminBtn = document.getElementById("adminButton");
let courseContainer = document.getElementById("courseContainer");
let cardfooter;

if (isAdmin === false || isAdmin === null){

	adminBtn.innerHTML = null

}
else
{
	adminBtn.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class"btn btn-block btn-primary">
			add Course
			</a>
		</div>	
	`
}

fetch ("http://localhost:3000/api/course/all",{
	method: "GET",
	headers:{
		"Authorization": `Bearer ${token}`
	}
})
.then (result => result.json())
.then (result => {

	if(result.length < 1){
		courseData = `No courses available`
	}
	else {
		courseData = result.map ((course) => {
			console.log(course);

			if(isAdmin === false || !isAdmin){
				cardfooter =
				`
					<a href="./singleCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block selectButton">
						SelectCourse
					</a>
				`
			}
			else {
				if(course.isActive == true){
					cardfooter =
					`
						<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit Course
						</a>

						<a href="./archiveCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block archiveButton">
							Archive Course
						</a>
					`
				}
				else
				{
					cardfooter =
					`
					<a href="./unarchiveCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block unarchiveButton">
						Un-archive Course
					</a>

					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block unarchiveButton">
						Delete
					</a>
					`
				}
				
			}

			return(
				`

					<div class="col-md-6 my-5">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
								${course.description}
								</p>
								<p class="card-text text-left">
								${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardfooter}
							</div>
						</div>
					</div>	

				`

				)
		}).join('')
	}

	let courseContainer = document.getElementById('courseContainer');

	courseContainer.innerHTML = courseData
})

// make a route to get single course
// make route to update the course
// make a route to archive the course
	// meaning update the status of the course from true to false
// make a route to un-archive the course
	// meaning update the status of the course from false to true
// make a route to delete a course 
