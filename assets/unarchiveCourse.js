
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/course/unarchiveCourse/${courseId}`,
	{
		method: "PUT",
		headers: {
			"Authorization": `Bearer ${token}`
		},
	}
)	
.then(result => result.json())
.then( result => {
	console.log(result)

	if(result){

		window.location.replace('./courses.html');
	} else {
		alert("Something went wrong");
	}
})