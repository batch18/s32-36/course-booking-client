//first name & last name
//email
//mobile number
//container
	// array of enrollemnts

	let lastName = document.querySelector('#lastName');
	let firstName = document.querySelector('#firstName');
	let email = document.querySelector('#email');
	let mobileNo = document.querySelector('#mobileNo');
	let enrollmentContainer = document.querySelector('#enrollmentContainer');

	let token = localStorage.getItem('token');

	fetch(`http://localhost:3000/api/users/details`,
		{
			method : "GET",
			headers : {	
				"Authorization": `Bearer ${token}`
			}
		}
	)
	.then( result => result.json())
	.then( result => {
		console.log(result)
		lastName.innerHTML = result.lastName;
		firstName.innerHTML = result.firstName;
		email.innerHTML = result.email;
		mobileNo.innerHTML = result.mobileNo;
		
		
	})